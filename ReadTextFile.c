void ReadTextFile() {
	delete h1;
	TH1F *h1 = new TH1F("h1","BGO spectrum;area Vs;number of events",250,-100e-9,0);
	TTree *t = new TTree("ntuple","x:y");
	t->ReadFile("filename.txt","x:y");
	t->Project("h1","x","y");
	h1->Draw();
}